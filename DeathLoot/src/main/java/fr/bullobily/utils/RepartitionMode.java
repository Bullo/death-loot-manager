package fr.bullobily.utils;

public enum RepartitionMode {
	//DROP_ALL,
	DISPATCH_BETWEEK_KILLERS,
	ALL_FOR_KILLER,
	NONE,
	;
	
	public static RepartitionMode fromString(String s) {
		for (RepartitionMode repart : RepartitionMode.values())
			if (repart.toString().equals(s.toUpperCase()))
				return repart;
		
		return NONE;
	}
}