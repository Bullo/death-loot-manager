package fr.bullobily.utils;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public enum Permissions {

	//USE_LOOT_BAG("use_bag"),
	BYPASS_LOOT_DROP("bypass"),
	RELOAD("reload")
	;
	
	private String perm;
	
	Permissions(String perm){
		this.perm = perm;
	}
	
	public boolean has(Player p) {
		return p == null ? false : p.hasPermission("deathmanager." + perm);
	}
	
	public boolean has(UUID p) {
		return has(Bukkit.getPlayer(p));
	}
}
