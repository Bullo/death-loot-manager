package fr.bullobily.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.util.TriConsumer;
import org.bukkit.Bukkit;
import org.bukkit.configuration.MemorySection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Sets.SetView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import fr.bullobily.DeathLootPlugin;
import fr.bullobily.objects.DamagedPlayer;
import fr.bullobily.objects.DeathLootListener;

public class ConfigEntry<T> {

	public static final ConfigEntry<RepartitionMode> LOOT_REPART_MODE = new ConfigEntry<RepartitionMode>("loot_attribution_mode", 
			RepartitionMode.DISPATCH_BETWEEK_KILLERS.toString(),
			(c, key) -> RepartitionMode.fromString(c.getString(key)), 
			(c, key, value) -> c.set(key, value.toString()));

	public static final ConfigEntry<Integer> DAMAGE_HISTORY_RETENTION = new ConfigEntry<Integer>("damage_history_retention", 
			200, 
			(c, key) -> c.getInt(key), 
			(c, key, value) -> c.set(key, value));

	public static final ConfigEntry<Boolean> APPLY_TO_PLAYERS = new ConfigEntry<Boolean>("apply_to_players",
			true, 
			(c, key) -> c.getBoolean(key), 
			(c, key, value) -> c.set(key, value));

	public static final ConfigEntry<Boolean> APPLY_TO_ENTITIES = new ConfigEntry<Boolean>("apply_to_entities", 
			false, 
			(c, key) -> c.getBoolean(key), 
			(c, key, value) -> c.set(key, value));

	public static final ConfigEntry<Boolean> PROTECT_DROPPED_LOOT = new ConfigEntry<Boolean>("protect_dropped_loot", 
			true, 
			(c, key) -> c.getBoolean(key), 
			(c, key, value) -> c.set(key, value));

	public static final ConfigEntry<Integer> PROTECT_DROPPED_LOOT_DURATION = new ConfigEntry<Integer>("protect_dropped_loot_duration", 
			400, 
			(c, key) -> c.getInt(key), 
			(c, key, value) -> c.set(key, value));

	public static final ConfigEntry<Boolean> SPAWN_DROPPED_LOOT_ON_KILLER = new ConfigEntry<Boolean>("spawn_dropped_loot_on_killer", 
			false, 
			(c, key) -> c.getBoolean(key), 
			(c, key, value) -> c.set(key, value));
	
	public static final ConfigEntry<Boolean> IS_LOOTBAG_LOOTABLE = new ConfigEntry<Boolean>("is_lootbag_lootable", 
			true, 
			(c, key) -> c.getBoolean(key), 
			(c, key, value) -> c.set(key, value));

	public static final ConfigEntry<Map<String, String>> MESSAGES = new ConfigEntry<Map<String, String>>("messages", 
			((Stream<String[]>)Stream.of(new String[][] { 
	        	{ Message.CANCELLED_LOOT_PROTECTED_PICKUP.toString().toLowerCase(), "§cThis loot is protected for %lootProtectionDurations for %lootOwner!" }, 
	        	{ Message.LOOT_ADDED_IN_BAG.toString().toLowerCase(), "§7You recieved a new loot in your bag. Occupied slots: %currentBagSize/%maxBagSize" },
	        	{ Message.LOOT_ADDED_IN_INVENTORY.toString().toLowerCase(), "§7You recieved a loot in your inventory." },  
	        	{ Message.LOOT_DROPPED.toString().toLowerCase(), "§7Some items have beed dropped for you." },
	        	{ Message.BAG_GUI_NAME.toString().toLowerCase(), "Lootbag" },
	        	{ Message.RELOAD_COMPLETE.toString().toLowerCase(), "§aDeathLoot have been successfully reloaded." },
	        	{ Message.EMPTY_LOOT_BAG.toString().toLowerCase(), "§cYour lootbag is empty." }, })).collect(Collectors.toMap(e -> e[0], e -> e[1])), 
			(c, key) -> {
				Map<String, String> map = new HashMap<String, String>();
				c.getConfigurationSection(key).getKeys(false).forEach(k -> map.put(k, c.getConfigurationSection(key).getString(k)));
				return map;
			}, 
			(c, key, value) -> c.set(key, value));

	/*public static final ConfigEntry<Map<UUID, List<ItemStack>>> STORED_ITEM_BAGS = new ConfigEntry<Map<UUID, List<ItemStack>>>("item_bags", 
			new TypeToken<Map<UUID, List<ItemStack>>>(){}.getType(), new HashMap<UUID, List<ItemStack>>());*/
	
	@SuppressWarnings("unchecked")
	public static final ConfigEntry<Map<UUID, List<ItemStack>>> ITEMBAGS = 
			new ConfigEntry<Map<UUID, List<ItemStack>>>("stored_items", 
					new HashMap<String, List<ItemStack>>(), 
					(c, key) ->  {
						Map<UUID, List<ItemStack>> map = new HashMap<UUID, List<ItemStack>>();
						//Bukkit.broadcastMessage("section : " + c.getConfigurationSection(key) + " // keys : " + c.getKeys(false));
						if (c.getConfigurationSection(key) == null)
							return map;
						
						c.getConfigurationSection(key).
						getKeys(false).
						forEach(k -> map.put(UUID.fromString(k), 
								(List<ItemStack>) c.getConfigurationSection(key).getList(k)));
						//((Map<String, List<ItemStack>>) ((MemorySection.get(key)))).entrySet().forEach(e -> map.put(UUID.fromString(e.getKey()), e.getValue()));
						return map;
					},
					(c, key, value) -> {
						value.entrySet().forEach(e -> c.set(key + "." + e.getKey().toString(), e.getValue()));
					});
	
	
	@SuppressWarnings({ "rawtypes" })
	public static void initConfig() {
		for (Field field : ConfigEntry.class.getFields())
		
			if (Modifier.isFinal(field.getModifiers()) && Modifier.isStatic(field.getModifiers())) {
				//DeathLootPlugin.getInstance().getConfig().getConfigurationSection("").getli
				ConfigEntry entry = null;
				try {
					entry = (ConfigEntry) field.get(null);
					if (!DeathLootPlugin.getInstance().getConfig().getKeys(true).contains(entry.configKey)) {
						DeathLootPlugin.getInstance().getConfig().set(entry.configKey, entry.defaultValue);
						DeathLootPlugin.getInstance().getLogger().warning("§eDefault value defined for " + entry.configKey);
					}
						//DeathLootPlugin.getInstance().getConfig().set(entry.configKey, entry.defaultValue);
					
				} catch (IllegalArgumentException | IllegalAccessException e) {
					DeathLootPlugin.getInstance().getLogger().warning("Error while loading default value for " + entry);
					e.printStackTrace();
				}
			}
		DeathLootPlugin.getInstance().saveConfig();				
	}
	
	private String configKey;
	private Object defaultValue;
	
	private BiFunction<FileConfiguration, String, T> getFromConfig;
	private TriConsumer<FileConfiguration, String, T> saveToConfig;
	
	private ConfigEntry(String key, Object defaultValue, 
			BiFunction<FileConfiguration, String, T> getFromConfig, 
			TriConsumer<FileConfiguration, String, T> saveToConfig) {
		this.defaultValue = defaultValue;
		this.configKey = key;
		this.getFromConfig = getFromConfig;
		this.saveToConfig = saveToConfig;
	}
	
	public T getValue() {
		return getFromConfig.apply(DeathLootPlugin.getInstance().getConfig(), configKey);
	}
	
	public void setValue(T value) {
		saveToConfig.accept(DeathLootPlugin.getInstance().getConfig(), configKey, value);
		DeathLootPlugin.getInstance().saveConfig();
		/*Gson gson = new Gson();
		DeathLootPlugin.getInstance().getConfig().set(configKey, gson.toJson(value));*/
	}
	
	public enum Message {
		CANCELLED_LOOT_PROTECTED_PICKUP(),
		LOOT_ADDED_IN_BAG(),
		LOOT_DROPPED(), 
		BAG_GUI_NAME(), 
		RELOAD_COMPLETE(), 
		EMPTY_LOOT_BAG(), 
		LOOT_ADDED_IN_INVENTORY(),
		;
		
		@SuppressWarnings("unchecked")
		public String get() {
			if (!MESSAGES.getValue().containsKey(toString().toLowerCase())) {
				Map<String, String> msgs = MESSAGES.getValue();
				msgs.put(toString().toLowerCase(), ((Map<String,String>) MESSAGES.defaultValue).get(toString().toLowerCase()));

				MESSAGES.setValue(msgs);
			}
			
			return MESSAGES.getValue().get(toString().toLowerCase());
		}
		
		public void send(LivingEntity target, ItemStack it) {
			String msg = get()
					.replace("%playerName", target.getName())
					.replace("%maxBagSize", ((DamagedPlayer)DamagedPlayer.get(target)).getMaxBagSize() + "")
					.replace("%currentBagSize", ((DamagedPlayer)DamagedPlayer.get(target)).getItemBag().size() + "");
			
			Player itemOwner = DeathLootListener.getDroppedItemData(it) == null ? null : Bukkit.getPlayer(DeathLootListener.getDroppedItemData(it).getValue());
			msg = msg.replace("%lootOwner", itemOwner == null ? "?" : itemOwner.getName());
			
			int protectionDuration = (int) (DeathLootListener.getDroppedItemData(it) == null ? 0 : 
				(ConfigEntry.PROTECT_DROPPED_LOOT_DURATION.getValue() - 
						(DeathLootPlugin.getInstance().currentTick() - DeathLootListener.getDroppedItemData(it).getKey())) / 20l + 1l);
			msg = msg.replace("%lootProtectionDuration", protectionDuration + "");
			
			target.sendMessage(msg);
			
		}
		
		public void send(LivingEntity target) {
			send(target, null);
		}
	}
}








