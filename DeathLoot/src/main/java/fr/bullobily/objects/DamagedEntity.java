package fr.bullobily.objects;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.collect.Comparators;

import fr.bullobily.DeathLootPlugin;
import fr.bullobily.api.DispatchLootEvent;
import fr.bullobily.utils.ConfigEntry;

public class DamagedEntity {
	
	//STATIC PART
	
	protected static long currentTick = 0;
	private static Map<UUID, DamagedEntity> entities = new HashMap<UUID, DamagedEntity>();
	
	static {
		Bukkit.getScheduler().runTaskTimer(DeathLootPlugin.getInstance(), () -> entities.values().forEach(e -> e.updateHistory(false)), 1, 1);
	}
	
	/**
	 * Return DamagedEntity for specified entity (or DamagedPlayer if the entity is a player). An instance is created if none exists yet
	 * @param e
	 * @return
	 */
	public static DamagedEntity get(Entity e) {
		if (!exists(e)) {
			DamagedEntity ent = e.getType() == EntityType.PLAYER ? new DamagedPlayer(e.getUniqueId()) : new DamagedEntity(e.getUniqueId());
			entities.put(e.getUniqueId(), ent);
			return ent;	
		}else
			return entities.get(e.getUniqueId());
	}
	
	public static Collection<DamagedEntity> getAll() {
		return Collections.unmodifiableCollection(entities.values());
	}
	
	/**
	 * Unregister DamagedEntity (if exists)
	 * @param e
	 * @return true if entity was registered, false otherwise
	 */
	public static boolean remove(Entity e) {
		if (!exists(e))
			return false;
		
		DamagedEntity ent = entities.remove(e.getUniqueId());
		if (!(ent instanceof DamagedPlayer))
			return true;
		
		//update item bag map in config
		DamagedPlayer p = (DamagedPlayer) ent;
		p.saveBagToConfig();
		return true;
	}
	
	/**
	 * Retuer whenether an entity has already a DamagedEntity instance linked to it or not
	 * @param e
	 * @return
	 */
	public static boolean exists(Entity e) {
		return entities.containsKey(e.getUniqueId());
	}
	
	
	//INSTANCE PART
	
	protected UUID uuid;
	protected Map<Long, List<Entry<DamagedPlayer, Double>>> history = new HashMap<Long, List<Entry<DamagedPlayer,Double>>>();
	
	public DamagedEntity(UUID uuid) {
		this.uuid = uuid;
	}
	
	public UUID getUUID() {
		return uuid;
	}
	
	public void addDamage(Player damager, double dmg) {
		if (!history.containsKey(currentTick))
			history.put(currentTick, new ArrayList<Entry<DamagedPlayer, Double>>());

		history.get(currentTick).add(new AbstractMap.SimpleEntry<DamagedPlayer, Double>((DamagedPlayer) get(damager), dmg));
	}
	
	private void updateHistory(boolean fullReset) {
		if (fullReset)
			history.clear();
		else
			for (Iterator<Long> i = history.keySet().iterator(); i.hasNext(); ) {
				long l = i.next();
				if (l + ConfigEntry.DAMAGE_HISTORY_RETENTION.getValue() < currentTick)
					i.remove();
			}
	}

	/**
	 * Execute drops dispatching. If entity is a player, a new instance is created. Otherwise it is just deleted. 
	 * No further call to any method of this instance should be done after calling dispatchDrops.
	 * @param drops
	 */
	public void dispatchDrops(Location deathLoc, List<ItemStack> drops) {
		entities.remove(getUUID());
		
		if (history.size() == 0)
			return;
		
		DamagedPlayer killer = history.get(history.keySet().stream().max(Comparator.naturalOrder()).get()).stream()
				.max(Comparator.comparingDouble(e -> e.getValue())).get().getKey();
			
		switch(ConfigEntry.LOOT_REPART_MODE.getValue()) {
		
		case ALL_FOR_KILLER:
			//(Map<DamagedPlayer, List<ItemStack>>)
			//((Stream<Object[]>)Stream.of(new Object[][] {{killer, drops}})).collect(Collectors.toMap(e -> e[0], e -> e[1]));
			
			DispatchLootEvent event = new DispatchLootEvent();
			event.addItems(killer, drops.toArray(new ItemStack[drops.size()]));
			Bukkit.getPluginManager().callEvent(event);
			
			event.getRepartition().forEach((p, its) -> p.addDrops(deathLoc, its.toArray(new ItemStack[its.size()])));
			
			drops.clear();
			break;
			
			
		case DISPATCH_BETWEEK_KILLERS:
			
			Map<DamagedPlayer, Double> scores = new LinkedHashMap<DamagedPlayer, Double>(); //contains the list of killers with additive probabilities
			Map<DamagedPlayer, List<ItemStack>> dispatchedDrops = new LinkedHashMap<DamagedPlayer, List<ItemStack>>(); 
			List<ItemStack> uniqueDrops = new ArrayList<ItemStack>();
			
			//determine participation scores for each player
			for (List<Entry<DamagedPlayer, Double>> list : history.values())
				for (Entry<DamagedPlayer, Double> e : list)
					if (Bukkit.getPlayer(e.getKey().getUUID()) != null && Bukkit.getPlayer(e.getKey().getUUID()).isOnline())
					if (!scores.containsKey(e.getKey()))
						scores.put(e.getKey(), e.getValue());
					else
						scores.put(e.getKey(), scores.get(e.getKey()) + e.getValue());	
			
			//translate in additive probabilities
			double totalScore = 0;
			for (Entry<DamagedPlayer, Double> e : scores.entrySet()) {
				e.setValue(e.getValue() + totalScore);
				totalScore = e.getValue();
			}
			
			//extraction of all unique items of the loot
			for (ItemStack it : drops)
				for (int i = 0 ; i < it.getAmount() ; i++) {
					ItemStack it2 = it.clone();
					it2.setAmount(1);
					uniqueDrops.add(it2);	
				}
			drops.clear();
			
			//create empty drops list for each player
			scores.forEach((key, value) -> dispatchedDrops.put(key, new ArrayList<ItemStack>()));
			
			//dispatch items between killers
			for (Iterator<ItemStack> i = uniqueDrops.iterator(); i.hasNext(); ) {
				ItemStack it = i.next();
				
				double d = ThreadLocalRandom.current().nextDouble(totalScore);
				
				for (Entry<DamagedPlayer, Double> e : scores.entrySet())
					if (e.getValue() > d && it != null) {
						dispatchedDrops.get(e.getKey()).add(it);
						it = null;
					}
			}
			DispatchLootEvent event2 = new DispatchLootEvent(dispatchedDrops);
			Bukkit.getPluginManager().callEvent(event2);
			event2.getRepartition().forEach((key, value) -> key.addDrops(deathLoc, value.toArray(new ItemStack[value.size()])));
			
			break;
			
		default:
			break;
		
		}
	}
	
	@Override
	public boolean equals(Object o) {
		return (o instanceof DamagedEntity) && ((DamagedEntity)(o)).getUUID().equals(uuid);
	}
}






