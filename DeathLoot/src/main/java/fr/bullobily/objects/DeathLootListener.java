package fr.bullobily.objects;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import fr.bullobily.DeathLootPlugin;
import fr.bullobily.api.EntityLootEvent;
import fr.bullobily.utils.ConfigEntry;
import fr.bullobily.utils.RepartitionMode;
import fr.bullobily.utils.ConfigEntry.Message;

public class DeathLootListener implements Listener {
	
	private static Map<ItemStack, Entry<Long, UUID>> protectedItems = new HashMap<ItemStack, Entry<Long,UUID>>();
	
	private Set<UUID> alreadyWarnedPlayerForCancelledPickup = new HashSet<UUID>();
	
	@EventHandler (priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onDamage(EntityDamageByEntityEvent e) {
		if (e.getDamager().getType() != EntityType.PLAYER || ConfigEntry.LOOT_REPART_MODE.getValue() == RepartitionMode.NONE)
			return;
		
		if (e.getEntityType() == EntityType.PLAYER && ConfigEntry.APPLY_TO_PLAYERS.getValue())
			DamagedEntity.get(e.getEntity()).addDamage((Player) e.getDamager(), e.getFinalDamage());
		
		else if (e.getEntityType() != EntityType.PLAYER && ConfigEntry.APPLY_TO_ENTITIES.getValue())
			DamagedEntity.get(e.getEntity()).addDamage((Player) e.getDamager(), e.getFinalDamage());
		
		Bukkit.getScheduler().runTaskTimer(DeathLootPlugin.getInstance(), 
				() -> protectedItems = protectedItems.entrySet().stream()
				.filter(entry -> entry.getValue().getKey() + ConfigEntry.PROTECT_DROPPED_LOOT_DURATION.getValue() > DeathLootPlugin.getInstance().currentTick())
				.collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue()))
				, 1, 1);
		
		Bukkit.getScheduler().runTaskTimer(DeathLootPlugin.getInstance(), () -> alreadyWarnedPlayerForCancelledPickup.clear(), 1, 100);
	}
	/*
	@EventHandler (priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onDamage(EntityDamageByBlockEvent e) {
		if (ConfigEntry.LOOT_REPART_MODE.getValue() != RepartitionMode.DEFAULT)
			DamagedEntity.getDamagedEntity(e.getEntity()).addDamage(null, e.getFinalDamage());
	}*/
	
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onDeath(EntityDeathEvent e) {
		if (!DamagedEntity.exists(e.getEntity()) || ConfigEntry.LOOT_REPART_MODE.getValue() == RepartitionMode.NONE)
			return;
		
		List<ItemStack> drops = e.getDrops();
		DamagedEntity ent = DamagedEntity.get(e.getEntity());
		
		if (ent instanceof DamagedPlayer && ConfigEntry.IS_LOOTBAG_LOOTABLE.getValue()) {
			drops.addAll(((DamagedPlayer)ent).getItemBag());
			((DamagedPlayer)ent).clearBag();
		}
		
		EntityLootEvent event = new EntityLootEvent(e.getEntity(), DamagedEntity.get(e.getEntity()), drops);
		Bukkit.getPluginManager().callEvent(event);
		
		if (!event.isCancelled())
			ent.dispatchDrops(e.getEntity().getLocation(), event.getLoot());
	}
	
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onItemPickup(EntityPickupItemEvent e) {
		ItemStack it = e.getItem().getItemStack();
		if (protectedItems.containsKey(it) && !protectedItems.get(it).getValue().equals(e.getEntity().getUniqueId())) {
			e.setCancelled(true);
			
			if (!alreadyWarnedPlayerForCancelledPickup.contains(e.getEntity().getUniqueId()))
				//e.getEntity().sendMessage(Message.CANCELLED_LOOT_PROTECTED_PICKUP.get().replace("%owner", Bukkit.getPlayer(protectedItems.get(it).getValue()).getDisplayName()));
				Message.CANCELLED_LOOT_PROTECTED_PICKUP.send(e.getEntity(), it);
			
			alreadyWarnedPlayerForCancelledPickup.add(e.getEntity().getUniqueId());
		}
	}
	
	/**
	 * Returns owner's UUID and protection duration of dropped item. Returns null if item isn't protected
	 * @param it
	 * @return
	 */
	public static Entry<Long, UUID> getDroppedItemData(ItemStack it) {
		return !protectedItems.containsKey(it) ? null : protectedItems.get(it);
	}
	
	public static void dropItem(Location deathLoc, Player killer, ItemStack... its) {
		if (its.length == 0)
			return;
		
		if (ConfigEntry.PROTECT_DROPPED_LOOT.getValue())
			Message.LOOT_DROPPED.send(killer);
		
		for (ItemStack it : its) {
			if (ConfigEntry.PROTECT_DROPPED_LOOT.getValue())
				protectedItems.put(it, new AbstractMap.SimpleEntry<Long, UUID>(DeathLootPlugin.getInstance().currentTick(), killer.getUniqueId()));
			
			if (ConfigEntry.SPAWN_DROPPED_LOOT_ON_KILLER.getValue())
				killer.getWorld().dropItem(killer.getLocation(), it);
			else
				deathLoc.getWorld().dropItem(deathLoc, it);
		}
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		DamagedEntity.remove(e.getPlayer());
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent e) {
		if (e.getClickedInventory() == null)
			return;
		
		DamagedPlayer p = (DamagedPlayer) DamagedEntity.get(e.getWhoClicked());
		
		if (e.getWhoClicked().getOpenInventory() == null ? true : !e.getWhoClicked().getOpenInventory().equals(p.getBagView()))
			return;
		e.setCancelled(true);
		
		/*if (e.getClickedInventory() == null ? true : e.getClickedInventory().equals(p.getBagInv()))
			return;*/
		
		/*if (!e.getWhoClicked().getOpenInventory().equals(p.getItembagGUI().get)e.getClickedInventory().equals(p.getItembagGUI()))
			return;*/

		if (e.getClick() == ClickType.LEFT && e.getClickedInventory() == null ? false : e.getClickedInventory().equals(p.getBagInv()))
			p.removeItemFromBag(e.getRawSlot());
		/*if (e.getCurrentItem() == null || !) 
			e.setCancelled(true);*/
	}
}
