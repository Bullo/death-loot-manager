package fr.bullobily.objects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.permissions.PermissionAttachmentInfo;

import fr.bullobily.DeathLootPlugin;
import fr.bullobily.utils.ConfigEntry;
import fr.bullobily.utils.ConfigEntry.Message;
import fr.bullobily.utils.Permissions;

public class DamagedPlayer extends DamagedEntity implements Listener {

	private static ItemStack blankItem = new ItemStack(Material.GRAY_STAINED_GLASS_PANE);
	
	static {
		ItemMeta meta = blankItem.getItemMeta();
		meta.setDisplayName(" ");
		blankItem.setItemMeta(meta);
	}

	
	private List<ItemStack> lootBag = new ArrayList<ItemStack>();
	private long lastTickCheckedBagSize = -1;
	private int bagSize = 0;
	private Inventory itemBagInv = null;
	private InventoryView itemBagView = null;
	
	public DamagedPlayer(UUID uuid) {
		super(uuid);
		if (ConfigEntry.ITEMBAGS.getValue().containsKey(uuid))
			lootBag = ConfigEntry.ITEMBAGS.getValue().get(uuid);
		
	}
	
	/**
	 * Add drops in player inventory or in his bag if he has enough place.
	 * @param it
	 */
	public void addDrops(Location deathLoc, ItemStack... it) {
		if (it.length == 0)
			return;
		
		Player p = Bukkit.getPlayer(uuid);
		
		Collection<ItemStack> unplacedItemsInInventory = p.getInventory().addItem(it).values();
		
		//Bukkit.broadcastMessage("it size : " + it.length + " // unplacedItems size : " + unplacedItemsInInventory.size());
		
		if (unplacedItemsInInventory.size() < it.length)
			Message.LOOT_ADDED_IN_INVENTORY.send(p);
		
		if (unplacedItemsInInventory.size() > 0 && getMaxBagSize() > 0) {
			Inventory inv = Bukkit.createInventory(null, getMaxBagSize());
			
			List<ItemStack> unplacedItemsInBag = new ArrayList<ItemStack>(inv.addItem(lootBag.toArray(new ItemStack[lootBag.size()])).values());
			unplacedItemsInBag.addAll(inv.addItem(unplacedItemsInInventory.toArray(new ItemStack[unplacedItemsInInventory.size()])).values());
			lootBag = new ArrayList<ItemStack>(Arrays.asList(inv.getContents())).stream().filter(item -> item != null).collect(Collectors.toList());
			
			if (unplacedItemsInBag.size() < unplacedItemsInInventory.size())
				Message.LOOT_ADDED_IN_BAG.send(p);
			
			DeathLootListener.dropItem(deathLoc, p, unplacedItemsInBag.toArray(new ItemStack[unplacedItemsInBag.size()]));
		}else 
			DeathLootListener.dropItem(deathLoc, p, unplacedItemsInInventory.toArray(new ItemStack[unplacedItemsInInventory.size()]));
	}
	
	/**
	 * Returns the max number of slots in the lootbag 
	 * @return
	 */
	public int getMaxBagSize() {
		if (currentTick == lastTickCheckedBagSize)
			return bagSize;
		
		Player p = Bukkit.getPlayer(uuid);
		
		lastTickCheckedBagSize = currentTick;
		bagSize = 0;
		
		for (Iterator<PermissionAttachmentInfo> i = p.getEffectivePermissions().iterator(); i.hasNext(); ) {
			PermissionAttachmentInfo perm = i.next();
			String[] splited = perm.getPermission().split("deathloot.bagsize.");
			if (perm.getValue() && splited.length == 2 && !splited[1].contains(".") && StringUtils.isNumeric(splited[1]))
				bagSize = Math.min(Math.max(bagSize, Integer.valueOf(splited[1])), 6) * 9;
		}
		
		return bagSize;
	}
	
	/**
	 * Return an unmodifiable view of items contained in the bag
	 * @return
	 */
	public List<ItemStack> getItemBag(){
		return Collections.unmodifiableList(lootBag);
	}
	
	public void clearBag() {
		lootBag.clear();
	}

	/**
	 * Remove item from bag. Return if the bag contained the item, false otherwise
	 * @param it
	 * @return
	 */
	public void removeItemFromBag(int slot) {
		Player p = Bukkit.getPlayer(uuid);
		if (lootBag.contains(itemBagInv.getItem(slot)))
			if (p.getInventory().firstEmpty() >= 0) {
				p.getInventory().addItem(itemBagInv.getItem(slot));
				lootBag.remove(itemBagInv.getItem(slot)); 
				itemBagInv.setItem(slot, blankItem);
			}
			
		/*if (itemBagInv.contains(it) && lootBag.contains(it))
			if (Bukkit.getPlayer(uuid).getInventory().addItem(it).values().size() == 0) {
				itemBagInv.setItem(itemBagInv.first(it), blankItem);
				lootBag.remove(it);
			}*/
	}
	
	public void showItemBagGUI() {
		if (lootBag.size() == 0) {
			Message.EMPTY_LOOT_BAG.send(Bukkit.getPlayer(uuid));
			return;
		}
		
		itemBagInv = Bukkit.createInventory(null, getMaxBagSize(), Message.BAG_GUI_NAME.get());
		itemBagInv.addItem(lootBag.toArray(new ItemStack[lootBag.size()]));
		for (int i = lootBag.size() ; i < getMaxBagSize() ; i++)
			itemBagInv.setItem(i, blankItem);
		
		itemBagView = Bukkit.getPlayer(uuid).openInventory(itemBagInv);
	}
	
	public Inventory getBagInv() {
		return itemBagInv;
	}
	
	public InventoryView getBagView() {
		return itemBagView;
	}
	
	public void saveBagToConfig() {
		Map<UUID, List<ItemStack>> map = ConfigEntry.ITEMBAGS.getValue();
		if (getItemBag().size() > 0)
			if (map.containsKey(uuid))
				map.get(uuid).addAll(getItemBag());
			else
				map.put(uuid, new ArrayList<ItemStack>(getItemBag()));
		else
			map.remove(uuid);
		
		ConfigEntry.ITEMBAGS.setValue(map);
	}
}





