package fr.bullobily;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import fr.bullobily.objects.DamagedEntity;
import fr.bullobily.objects.DamagedPlayer;
import fr.bullobily.objects.DeathLootListener;
import fr.bullobily.utils.ConfigEntry;

public class DeathLootPlugin extends JavaPlugin {
	
	private static DeathLootPlugin plugin;
	
	public static DeathLootPlugin getInstance() {
		return plugin;
	}
	
	private long currentTick = 0;
	
	public long currentTick() {
		return currentTick;
	}
	
	@Override
	public void onEnable() {
		plugin = this;
		ConfigEntry.initConfig();
		getServer().getPluginManager().registerEvents(new DeathLootListener(), this);
		
		DLCommand dlCmd = new DLCommand();
		getCommand("dl").setExecutor(dlCmd);
		getCommand("dl").setTabCompleter(dlCmd);
		
		Bukkit.getScheduler().runTaskTimer(this, () -> currentTick++, 1, 1);
	}
	
	@Override
	public void onDisable() {
		DamagedEntity.getAll().stream().filter(de -> de instanceof DamagedPlayer).forEach(de -> ((DamagedPlayer)de).saveBagToConfig());
	}	
}
