package fr.bullobily.api;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Entity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

import fr.bullobily.objects.DamagedEntity;

/**
 * Called when an entity dies. It defines the list of items to spread between killers. If cancelled, the loot will be dropped on the floor as in vanilla.
 * @author Bullobily
 *
 */
public class EntityLootEvent extends Event implements Cancellable { 
	
	private static final HandlerList HANDLERS = new HandlerList();
	private boolean isCancelled;
	private Entity killed;
	private DamagedEntity damagedEntity;
	private List<ItemStack> loot;
	
	 
	@Override
	public HandlerList getHandlers() {
	    return HANDLERS;
	}
	 
	public static HandlerList getHandlerList() {
	    return HANDLERS;
	}

	@Override
	public boolean isCancelled() {
		return isCancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		isCancelled = cancelled;
	}

	public EntityLootEvent(Entity killed, DamagedEntity damagedEntity, List<ItemStack> loot) {
		this.killed = killed;
		this.damagedEntity = damagedEntity;
		this.loot = loot;
	}

	public Entity getKilled() {
		return killed;
	}

	public DamagedEntity getDamagedEntity() {
		return damagedEntity;
	}

	public List<ItemStack> getLoot() {
		return loot;
	}

	public void setLoot(List<ItemStack> loot) {
		this.loot = loot == null ? new ArrayList<ItemStack>() : loot;
	}
}





