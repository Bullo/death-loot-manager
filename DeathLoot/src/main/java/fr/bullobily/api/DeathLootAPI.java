package fr.bullobily.api;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import fr.bullobily.objects.DamagedEntity;
import fr.bullobily.objects.DamagedPlayer;

public abstract class DeathLootAPI {

	private DeathLootAPI() {
	}
	
	/**
	 * Returns the DamagedEntity instance for the entity. 
	 * <br>If forceCreation is false, result may be null (if the entity has never been damaged). 
	 * If it's true, a new DamagedEntity instance might be created. 
	 * @param entity
	 * @param forceCreation
	 * @return
	 */
	public static DamagedEntity getDamagedEntity(Entity entity, boolean forceCreation) {
		return forceCreation || DamagedEntity.exists(entity) ? DamagedEntity.get(entity) : null;	
	}
	
	/**
	 * Returns the DamagedEntity instance for the entity. 
	 * <br>If forceCreation is false, result may be null (if the entity has never been damaged). 
	 * If it's true, a new DamagedEntity instance might be created. 
	 * @param entity
	 * @param forceCreation
	 * @return
	 */
	public static DamagedPlayer getDamagedPlayer(Player player, boolean forceCreation) {
		return forceCreation || DamagedEntity.exists(player) ? (DamagedPlayer) DamagedEntity.get(player) : null;	
	}
}





