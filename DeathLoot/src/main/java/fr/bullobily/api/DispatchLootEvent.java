package fr.bullobily.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

import fr.bullobily.objects.DamagedPlayer;

/**
 * Called after loot repartition calculation. Contains list of items for each reciever.
 * @author Bullobily
 *
 */
public class DispatchLootEvent extends Event {
	
	private static final HandlerList HANDLERS = new HandlerList();
	
	@Override
	public HandlerList getHandlers() {
	    return HANDLERS;
	}
	 
	public static HandlerList getHandlerList() {
	    return HANDLERS;
	}
	
	private Map<DamagedPlayer, List<ItemStack>> repartition = new HashMap<DamagedPlayer, List<ItemStack>>();

	public DispatchLootEvent() {
	}
	
	public DispatchLootEvent(Map<DamagedPlayer, List<ItemStack>> repartition) {
		this.repartition = repartition;
	}

	public Map<DamagedPlayer, List<ItemStack>> getRepartition() {
		return repartition;
	}

	public void setRepartition(Map<DamagedPlayer, List<ItemStack>> repartition) {
		this.repartition = repartition;
	}
	
	public void addItems(DamagedPlayer player, ItemStack... items) {
		if (!repartition.containsKey(player))
			repartition.put(player, new ArrayList<ItemStack>(Arrays.asList(items)));
		else
			repartition.get(player).addAll(Arrays.asList(items));
	}
	
	
	
}



