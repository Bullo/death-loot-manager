package fr.bullobily;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.google.common.collect.ImmutableList;

import fr.bullobily.objects.DamagedEntity;
import fr.bullobily.objects.DamagedPlayer;
import fr.bullobily.utils.ConfigEntry;
import fr.bullobily.utils.ConfigEntry.Message;
import fr.bullobily.utils.Permissions;

public class DLCommand implements CommandExecutor, TabCompleter {
	
	private List<String> inTab = ImmutableList.of("reload", "loot");

	@Override
	public boolean onCommand(CommandSender sender, Command command, String cmd, String[] args) {
		if (args.length == 0 || !(sender instanceof Player))
			return false;
		
		switch(args[0]) {
		case "reload":
			if (!Permissions.RELOAD.has((Player) sender))
				return false;
			
			DeathLootPlugin.getInstance().reloadConfig();
			ConfigEntry.initConfig();
			Message.RELOAD_COMPLETE.send((LivingEntity) sender);
			break;
			
		case "loot":
			((DamagedPlayer)DamagedEntity.get((Entity) sender)).showItemBagGUI();
			break;
		}
		
		return false;
	}

	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String cmd, String[] args) {
		return inTab.stream().filter(s -> s.startsWith(args[0])).collect(Collectors.toList());
	}
	
}
